package Dbutils

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func GetConnection(user string, password string, dburl string, dbname string, port string) *sql.DB {
	db, err := sql.Open("mysql", user+":"+password+"@tcp("+dburl+":"+port+")/"+dbname)
	if err != nil {
		panic(err)
	}
	return db
}

func ExecuteUpdateQuery(db *sql.DB, query string) (status int) {
	fmt.Println("going to update with Query " + query)
	//db, err := sql.Open("mysql", user+"@:"+password+"@tcp("+dburl+":"+port+")/"+dbname)
	// if err != nil {
	// 	panic(err)
	// }
	_, queryerr := db.Query(query)
	if queryerr != nil {
		status = 0
		panic(queryerr)
	} else {
		status = 1
	}
	//defer db.Close()
	return status
}

func GetSingleColumnList(db *sql.DB, query string) []string {
	fmt.Println("going to getSingleColumnList " + query)
	rows, queryerr := db.Query(query)
	if queryerr != nil {
		fmt.Println(queryerr)
		panic("this")
	}
	//counter := 0
	var argList []string
	for rows.Next() {
		var element string
		rows.Scan(&element)
		//fmt.Println("printing element")
		//fmt.Println(element)
		argList = append(argList, element)
	}
	return argList
}

func GetMultiValueList(db *sql.DB, query string) map[string][]string {
	rows, err := db.Query(query)
	if err != nil {
		fmt.Println("Failed to run query", err)
	}
	cols, err := rows.Columns()
	if err != nil {
		fmt.Println("Failed to get columns", err)
	}
	fmt.Println("Printing Multivalue Query" + query)
	rawResult := make([][]byte, len(cols))
	result := make([]string, len(cols))
	dest := make([]interface{}, len(cols)) // A temporary interface{} slice
	retMap := make(map[string][]string)
	for i, _ := range rawResult {
		dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}
	counter := 0
	for rows.Next() {
		err = rows.Scan(dest...)
		//fmt.Println("running outer Loop " + strconv.Itoa(counter))
		counter++
		if err != nil {
			fmt.Println("Failed to scan row", err)
		}
		for i, raw := range rawResult {

			if raw == nil {
				result[i] = "\\N"
			} else {
				result[i] = string(raw)
				retMap[cols[i]] = append(retMap[cols[i]], result[i])
			}
		}
		//fmt.Printf("%#v\n", result)
	}
	// for r, _ := range result {
	// 	fmt.Println("printing final results" + result[r])
	// }
	// fmt.Println("PRINTING FINAL RETMAP")
	// for i, j := range retMap {
	// 	fmt.Println(i)
	// 	fmt.Print(j)
	// 	fmt.Println("NEXT")
	// 	fmt.Println("len of j is " + strconv.Itoa(len(j)))
	// }
	return retMap
}

func CloseConnection(db *sql.DB) {
	defer db.Close()
}
