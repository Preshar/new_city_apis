package main

import (
	"bitbucket.org/new_city_apis/delogin"
	"bitbucket.org/new_city_apis/nc"
	"bitbucket.org/new_city_apis/serviceability"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("test")
	router := mux.NewRouter()
	router.HandleFunc("/validatecity", nc.Validatecity).Methods("GET")
	router.HandleFunc("/delogin", delogin.Delogin).Methods("GET")
	router.HandleFunc("/showreststatus", serviceability.GetRestStatus).Methods("GET")
	log.Fatal(http.ListenAndServe(":8001", router))
}
