package nc

import (
	"bitbucket.org/new_city_apis/DbUtils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

/*
workflow
check wether city is open in cms
check wether city_rain_params exists
check wether delivery ckonfigs exists
Check wether testzone exists for the given city
Check wether test delivery boys exist fro a new city
Just do this this one for complete flow
select r.name,r.slug,rp.rest_id,rp.type_of_partner,r.enabled,r.city_code,r.area_code
from restaurants r inner join restaurants_partner_map rp on r.id=rp.`rest_id`
where city_code=105 and enabled =1 and name like '%test%';
*/

type PartnerResponse struct {
	Restid   string `json:"restaurant_id"`
	Ptype    string `json:"partner_type"`
	Restname string `json:"restaurant_name"`
}

type AllResp struct {
	Restpat              []PartnerResponse `json:"restaurant_partner_mapping"`
	Opencity             bool              `json :"is_city_open"`
	Enabledcity          bool              `json :"is_city_enabled"`
	Polyenabled          bool              `json:"is_polygon_enabled"`
	Polymapenabled       bool              `json:"is_polygon_mapping_enabled"`
	DelConfigStatus      bool              `json:"delivery_config_status"`
	AutoAssignStatus     bool              `json:"delivery_auto_assign_status"`
	TestZone             string            `json:"test_zone_id"`
	ZoneEnabled          bool              `json:"is_testzone_enabled"`
	ZoneOpen             bool              `json:"is_testzone_open"`
	ZoneRainParamsStatus bool              `json:"zone_rain_params_status"`
	CityRainParams       bool              `json:"city_rain_params_status"`
	DeliveryBoys         []string          `json:"delivery_boys_available"`
}

// func main() {
// 	router := mux.NewRouter()
// 	router.HandleFunc("/validatecity", validatecity).Methods("GET")

// 	log.Fatal(http.ListenAndServe(":8001", router))
// }

func Validatecity(w http.ResponseWriter, r *http.Request) {
	cityId := r.URL.Query()["city_id"][0]
	var pr []PartnerResponse
	var fullResponse AllResp
	cmscon := Dbutils.GetConnection("swiggy", "swiggy@2014", "backend-db-p-slave-01.swiggyops.de", "swiggy", "3306")
	deliverycon := Dbutils.GetConnection("delivery-mkt", ">{j8,k{T2.g!Jtt!g;tL", "de-p-marketing-slave-01.swiggyops.de", "swiggy", "3306")
	query := "select r.name,rp.rest_id,rp.type_of_partner  from restaurants r inner join restaurants_partner_map rp on r.id=rp.rest_id  where city_code=" + cityId + " and enabled =1 and name like '%test%'"
	//"select r.name,r.slug,rp.rest_id,rp.type_of_partner,r.enabled,r.city_code,r.area_code  from restaurants r inner join restaurants_partner_map rp on r.id=rp.rest_id  where city_code=120 and enabled =1 and name like '%test%'"
	fmt.Println(query)
	cmsslice := Dbutils.GetMultiValueList(cmscon, query)
	fmt.Println(cmsslice)
	pr = getRestPartMapping(cmsslice)
	fullResponse.Restpat = pr
	enablecity, opencity := checkCityOpen(cmscon, cityId)
	fullResponse.Opencity = enablecity
	fullResponse.Enabledcity = opencity
	polygonenabled, polygonmappingenabled := getSandPolygonStatus(cmscon, cityId)
	fullResponse.Polyenabled = polygonenabled
	fullResponse.Polymapenabled = polygonmappingenabled
	deliverycofigstatus := checkdeliveryConfigstatus(deliverycon, cityId)
	fullResponse.DelConfigStatus = deliverycofigstatus
	autoassignstatus := checkautoAssign(deliverycon, cityId)
	fullResponse.AutoAssignStatus = autoassignstatus
	testzone := getTestZone(deliverycon, cityId)
	fullResponse.TestZone = testzone
	if testzone != "" {
		zenabled, zopen := isZoneOpenEnabled(deliverycon, testzone)
		fullResponse.ZoneEnabled = zenabled
		fullResponse.ZoneOpen = zopen
	}
	zrp := checkzoneRainParams(deliverycon, cityId)
	fullResponse.ZoneRainParamsStatus = zrp
	dboys := getallDeliveryBoys(deliverycon, cityId)
	fullResponse.DeliveryBoys = dboys
	crparamsstatus := checkCityRainParams(deliverycon, cityId)
	fullResponse.CityRainParams = crparamsstatus
	fmt.Println(fullResponse)
	b, _ := json.Marshal(fullResponse)
	fmt.Println(b)
	fmt.Println(string(b))
	finalresponse := string(b)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, finalresponse)

	//getSandPolygonStatus(cmscon, "120")
	//someflag := checkdeliveryConfigstatus(deliverycon, "120")
	//fmt.Println(someflag)
	//someflag := checkautoAssign(deliverycon, "24")
	//x := getallDeliveryBoys(deliverycon, "110")
	//fmt.Println(x)

	// getSandPolygonStatus(cmscon,"110")
	// checkdeliveryConfigstatus(deliverycon,"110")
	// checkautoAssign(deliverycon,"110")
	// getTestZone(deliverycon,"110")
	// checkzoneRainParams(deliverycon,"110")
	// getallDeliveryBoys(deliverycon,"110")
}

func getRestPartMapping(cmsMap map[string][]string) []PartnerResponse {
	restlen := len(cmsMap["rest_id"])
	var parray []PartnerResponse
	fmt.Println(restlen)
	for i := 0; i < restlen; i++ {
		fmt.Println(cmsMap["rest_id"][i] + "   " + cmsMap["type_of_partner"][i])
		var partner PartnerResponse
		partner.Restid = cmsMap["rest_id"][i]
		partner.Ptype = cmsMap["type_of_partner"][i]
		partner.Restname = cmsMap["name"][i]
		parray = append(parray, partner)
	}
	//fmt.Println(parray)
	return parray
}

func checkCityOpen(cmscon *sql.DB, cityid string) (bool, bool) {
	query := "select enabled, is_open from city where id =" + cityid
	enMap := Dbutils.GetMultiValueList(cmscon, query)
	var openFlag bool
	var enableFlag bool
	if enMap["enabled"][0] == "1" {
		enableFlag = true
	} else {
		enableFlag = false
	}
	if enMap["is_open"][0] == "1" {
		openFlag = true
	} else {
		openFlag = false
	}
	//fmt.Println(openFlag)
	//fmt.Println(enableFlag)
	return enableFlag, openFlag
}

func getSandPolygonStatus(cmscon *sql.DB, cityid string) (bool, bool) {
	polygonmappingenabled := false
	polygonenabled := false
	pollmapquery := "select city_id,polygon_id from city_polygon where city_id =" + cityid
	sanMap := Dbutils.GetMultiValueList(cmscon, pollmapquery)
	if len(sanMap) > 0 {
		polygonmappingenabled = true
		polygon_id := sanMap["polygon_id"][0]
		pollenabledQuery := "select enabled from polygon where id=" + polygon_id
		polList := Dbutils.GetSingleColumnList(cmscon, pollenabledQuery)
		if polList[0] == "1" {
			polygonenabled = true
		}
		//fmt.Println(polygonmappingenabled)
		//fmt.Println(polygonenabled)
	}
	return polygonenabled, polygonmappingenabled
}

//func checkZoneRainParams(cmscon *sql.DB, cityid string) (bool, bool) {
//	zoneQuery := ""
//take out zone with name test from the city id

//once found find out all the test des mapped to this zone which are enabled with a given employment status

//find out if this zone has zone_rain_params set for all rain mode state

//Check city_rain_params for this particular city
//}

func checkCityRainParams(deliverycon *sql.DB, cityId string) bool {
	//
	crQuery := "select count(*) from city_rain_params where city_id =" + cityId
	count := Dbutils.GetSingleColumnList(deliverycon, crQuery)
	city_rain_params := false
	fmt.Println("Preetesh Printing value " + count[0])
	if count[0] == "1" {
		city_rain_params = true
	}
	return city_rain_params
}

func checkdeliveryConfigstatus(deliverycon *sql.DB, cityId string) bool {
	new_keys_list := []string{"rain_mode_banner_city_", "PEAK_TIME_FOR_CITY_", "show_incentives_", "online_contracts_enabled_", "use_callme_", "show_customer_bf_confirm_", "OE_INFINITY_NUMBER_", "call_customer_partner", "DYNAMIC_CASHLIMIT_CITY_", "DP_FEATURE_GATE_", "geofence_enabled_", "de_assistance_enabled_", "SUSPENSION_LOGIN_DAYS_LIMIT_PDP_", "SUSPENSION_LOGIN_DAYS_LIMIT_TEMP_", "SUSPENSION_LOGIN_DAYS_LIMIT_FTC_", "SUSPENSION_LOGIN_DAYS_LIMIT_3PL_", "ABSCONDING_LOGIN_DAYS_LIMIT_PDP_", "ABSCONDING_LOGIN_DAYS_LIMIT_TEMP_", "ABSCONDING_LOGIN_DAYS_LIMIT_FTC_", "ABSCONDING_LOGIN_DAYS_LIMIT_3PL_", "de_referral_", "referral_sms_"}
	//old_keys_list := []string{"society_switch_city", "LM_DIST_FEEDBACK_SHOW", "AUTO_REJECT_CRON_SWITCH", "banner_city_id_list", "dominos_city_delivery_minutes", "SLA_CONTROLLED_RELEASE_CITY", "new_user_city", "polygon_switch", "network_force_logout_enabled_cities", "new_batching_cx_cities", "allowed_cities_for_cas", "auto_reject_cities", "distance_calculation_cities", "force_logout_enabled_cities", "process_call_me_cities", "enabled_cities_incentive_notification", "history_v2_enabled_cities", "ticketing_enabled_cities", "Banner_logic_city_list", "task_sequencing_enabled_for_cities", "LONG_DISTANCE_CITIES", "AUTOMATED_SUSPENSION_CITIES", "AUTOMATED_ABSCONDED_CITIES", "login_history_enabled_cities"}
	for i := range new_keys_list {
		new_keys_list[i] = new_keys_list[i] + cityId
	}
	//fmt.Println(strings.Join(new_keys_list[:], ","))
	configlist := "'" + strings.Replace(strings.Join(new_keys_list[:], ","), ",", "','", -1) + "'"
	fmt.Println(configlist)
	somelist := Dbutils.GetSingleColumnList(deliverycon, "select count(*) from delivery_config where meta_key in ("+configlist+")")
	//somelist := Dbutils.GetSingleColumnList(deliverycon, "select count(*) from delivery_config where meta_key in ('rain_mode_banner_city_120')")
	//fmt.Println(somelist)
	if somelist[0] == strconv.Itoa(len(new_keys_list)) {
		return true
	} else {
		return false
	}
}

func checkautoAssign(deliverycon *sql.DB, cityId string) bool {
	query := "select meta_value from delivery_config where meta_key like '%auto_assign_partition_cities_%'"
	somelist := Dbutils.GetSingleColumnList(deliverycon, query)
	someflag := false
	for i := range somelist {
		if strings.Contains(somelist[i], ",") {
			rowlist := strings.Split(somelist[i], ",")
			for j := range rowlist {
				if cityId == rowlist[j] {
					someflag = true
					fmt.Println("In first loop")
					break
				}
			}
		} else {
			if cityId == somelist[i] {
				someflag = true
				fmt.Println("In second loop")
				break
			}
		}
	}
	if someflag {
		return true
	}
	return false
}

func getTestZone(deliverycon *sql.DB, cityId string) string {
	query := "select id from zone where name like '%test%' and city_id=" + cityId + " and enabled =1"
	somelist := Dbutils.GetSingleColumnList(deliverycon, query)
	if len(somelist) > 0 {
		return somelist[0]
	} else {
		return "nil"
	}
}

func checkzoneRainParams(deliverycon *sql.DB, cityId string) bool {
	zrflag := false
	//zone := getTestZone(deliverycon, cityId)
	zrquery := "select distinct(zone_id) from zone_rain_params where id in (select id from zone where city_id =" + cityId + ") and rain_mode=4"
	zonequery := "select id from zone where city_id =" + cityId
	zrlist := Dbutils.GetSingleColumnList(deliverycon, zrquery)
	zlist := Dbutils.GetSingleColumnList(deliverycon, zonequery)
	if len(zlist) == len(zrlist) {
		zrflag = true
	}
	return zrflag
}

func getallDeliveryBoys(deliverycon *sql.DB, cityId string) []string {
	zone_id := getTestZone(deliverycon, cityId)
	dquery := "select id from delivery_boys where zone_id in (select id from zone where city_id =" + cityId + " and name like '%test%') and zone_id =" + zone_id + " and enabled=1 and employment_status=1"
	somelist := Dbutils.GetSingleColumnList(deliverycon, dquery)
	return somelist
}

func isZoneOpenEnabled(deliverycon *sql.DB, zone_id string) (bool, bool) {
	dquery := "select enabled, is_open from zone where id =" + zone_id
	zslice := Dbutils.GetMultiValueList(deliverycon, dquery)
	var enabled bool
	var open bool
	if len(zslice) == 0 {
		return enabled, open
	}
	fmt.Println("Preetesh Printing zslice")
	fmt.Println(zslice)
	if zslice["enabled"][0] == "1" {
		enabled = true
	}
	if zslice["is_open"][0] == "1" {
		open = true
	}
	return enabled, open

}

//check zone rain params

//check delivery boys

//check city Id exists in delivery and is enabled

//#################FINAL
//check cityid exists in cms and is enabled
//get the zone id with name as test in delivery
//select * from zone where name like '%test%' and city_id=90 and enabled =1
