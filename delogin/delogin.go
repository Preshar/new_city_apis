package delogin

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type allLoginResponse struct {
	DeLoginStatus string `json:"deLoginStatus"`
	DeOtp         string `json:"deOtp"`
	DeAuth        string `json:"deAuth"`
	DelatLon      string `json:"delatLon"`
	Deid          string `json:"deid"`
}

// func main() {
// 	router := mux.NewRouter()
// 	router.HandleFunc("/people", GetPeople).Methods("GET")
// 	log.Fatal(http.ListenAndServe(":8001", router))

// }

func Delogin(w http.ResponseWriter, r *http.Request) {
	endp := r.URL.Query()["url"][0]
	endp = "http://" + endp
	fmt.Println("got url as" + endp)
	deid := r.URL.Query()["deid"][0]
	fmt.Println("got deid as" + deid)
	latlon := r.URL.Query()["latlon"][0]
	lat := strings.Trim(strings.Split(latlon, ",")[0], " ")
	lon := strings.Trim(strings.Split(latlon, ",")[1], " ")
	latlon = lat + "," + lon
	fmt.Println("got latlon as" + latlon)
	var otp string
	var auth string
	var locstatus bool
	logstatus := deLogin(endp, deid)
	if logstatus == false {
		otp = "nil"
		auth = "nil"
		locstatus = false
	} else {
		otp = deGetOtp(endp, deid)
		auth = deOtp(endp, deid, otp)
		//	fmt.Println("auth of de is" + auth)
		locstatus = deLocationUpdate(endp, latlon, auth)
		updatecontract(auth, endp)
		fmt.Println(locstatus)
	}
	var lg allLoginResponse
	lg.DeAuth = auth
	fmt.Println(auth)
	lg.DeLoginStatus = strconv.FormatBool(logstatus)
	fmt.Println(logstatus)
	lg.DeOtp = otp
	fmt.Println(otp)
	lg.Deid = deid
	fmt.Println(deid)
	lg.DelatLon = latlon
	fmt.Println(latlon)
	b, err := json.Marshal(lg)
	fmt.Println("Printing final json")
	fmt.Println(string(b))
	fmt.Println(err)
	finalresponse := string(b)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, finalresponse)

}

func deLogin(endpoint string, de_id string) bool {
	req, err := http.NewRequest("POST", endpoint+"/api/delivery_boy/login?id="+de_id+"&version=1.9", nil)
	if err != nil {
		fmt.Println("printing err")
		fmt.Println(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=")
	resp, err := http.DefaultClient.Do(req)
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	fmt.Println(bodyString)
	var logres allLoginResponse
	err3 := json.Unmarshal([]byte(bodyString), &logres)
	if err != nil {
		fmt.Println(err3)
	}
	fmt.Println(logres)
	var result map[string]interface{}
	json.Unmarshal([]byte(bodyString), &result)
	statuscode := result["statusMessage"].(string)
	if strings.EqualFold(statuscode, "OTP Sent") {
		fmt.Println("true")
		return true
	} else {
		fmt.Println("false")
		return false
	}

}

func deGetOtp(endpoint string, deid string) string {
	var result map[string]interface{}
	req, err := http.NewRequest("GET", endpoint+"/services/delivery-boy/get-otp?deId="+deid, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=")
	resp, err := http.DefaultClient.Do(req)
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	json.Unmarshal([]byte(bodyString), &result)
	otp := result["data"].(float64)
	//return fmt.Sprintf("%g", otp)
	str := strconv.FormatFloat(otp, 'f', 0, 64)
	return str
}

func deOtp(endpoint string, deid string, otp string) string {
	var result map[string]interface{}
	req, err := http.NewRequest("POST", endpoint+"/api/delivery_boy/otp?id="+deid+"&otp="+otp, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=")
	resp, err := http.DefaultClient.Do(req)
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	json.Unmarshal([]byte(bodyString), &result)
	datamap := result["data"].(map[string]interface{})
	auth := datamap["Authorization"]
	fmt.Println(auth)
	return auth.(string)
}

func deLocationUpdate(endpoint string, latlon string, auth string) bool {
	var result map[string]interface{}
	lat := strings.Split(latlon, ",")[0]
	lng := strings.Split(latlon, ",")[1]
	req, err := http.NewRequest("POST", endpoint+"/api/delivery_boy/location?lat="+lat+"&lng="+lng, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", auth)
	resp, err := http.DefaultClient.Do(req)
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	json.Unmarshal([]byte(bodyString), &result)
	statuscode := result["statusMessage"].(string)
	fmt.Println(statuscode)
	if strings.EqualFold(statuscode, "Success") {
		return true
	} else {
		return false
	}

}

func updatecontract(deauth string, endpoint string) {
	req_bank, _ := http.NewRequest("POST", endpoint+"/api/delivery_boy/update_signed_contract?contract_type=bank_details", nil)
	req_bank.Header.Set("Authorization", deauth)
	fmt.Println("calling bank")
	req_gen, _ := http.NewRequest("POST", endpoint+"/api/delivery_boy/update_signed_contract?contract_type=general_terms", nil)
	req_gen.Header.Set("Authorization", deauth)
	fmt.Println("calling general")
	req_coc, _ := http.NewRequest("POST", endpoint+"/api/delivery_boy/update_signed_contract?contract_type=code_of_conduct", nil)
	req_coc.Header.Set("Authorization", deauth)
	fmt.Println("calling coc")
	req_pay, _ := http.NewRequest("POST", endpoint+"/api/delivery_boy/update_signed_contract?contract_type=payouts", nil)
	req_pay.Header.Set("Authorization", deauth)
	fmt.Println("calling payout")
	resp_bank, err_bank := http.DefaultClient.Do(req_bank)
	if err_bank != nil {
		fmt.Println(err_bank)
	}
	body_bank, _ := ioutil.ReadAll(resp_bank.Body)
	fmt.Println(string(body_bank))

	resp_gen, err_gen := http.DefaultClient.Do(req_gen)
	if err_gen != nil {
		fmt.Println(err_gen)
	}
	body_gen, _ := ioutil.ReadAll(resp_gen.Body)
	fmt.Println(string(body_gen))

	resp_coc, err_coc := http.DefaultClient.Do(req_coc)
	if err_coc != nil {
		fmt.Println(err_coc)
	}
	body_coc, _ := ioutil.ReadAll(resp_coc.Body)
	fmt.Println(string(body_coc))

	resp_pay, errpay := http.DefaultClient.Do(req_pay)
	if errpay != nil {
		fmt.Println(errpay)
	}
	body_pay, _ := ioutil.ReadAll(resp_pay.Body)
	fmt.Println(string(body_pay))
}
