package serviceability

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

// type cerebroResponse struct {
// 	ServiceableRestaurants []ServiceableRestaurants
// }

// type ServiceableRestaurants []struct {
// 	RestaurantID                  string                        `json:"restaurantId"`
// 	AreaID                        int                           `json:"areaId"`
// 	CityID                        int                           `json:"cityId"`
// 	ListingServiceabilityResponse ListingServiceabilityResponse `json:"listingServiceabilityResponse"`
// 	CustomerInfo                  CustomerInfo                  `json:"customerInfo"`
// }

// type ListingServiceabilityResponse struct {
// 	RestaurantID              int         `json:"restaurant_id"`
// 	DeliveryTime              int         `json:"delivery_time"`
// 	MinDeliveryTime           int         `json:"min_delivery_time"`
// 	MaxDeliveryTime           int         `json:"max_delivery_time"`
// 	LastMileTravel            float64     `json:"last_mile_travel"`
// 	DistanceCalculationMethod string      `json:"distance_calculation_method"`
// 	Serviceability            int         `json:"serviceability"`
// 	RainMode                  string      `json:"rain_mode"`
// 	NonServiceableReason      int         `json:"non_serviceable_reason,omitempty`
// 	Degradation               Degradation `json:"degradation"`
// 	LongDistance              int         `json:"long_distance"`
// 	Batchable                 bool        `json:"batchable"`
// 	StartBannerFactor         float64     `json:"start_banner_factor"`
// 	StopBannerFactor          float64     `json:"stop_banner_factor"`
// 	DemandShapingFactor       float64     `json:"demand_shaping_factor"`
// 	BannerFactor              float64     `json:"banner_factor"`
// 	BannerFactorDiff          int         `json:"banner_factor_diff"`
// 	ZoneBeefup                int         `json:"zone_beefup"`
// 	RainBeefup                int         `json:"rain_beefup"`
// 	ThirtyMinsOrFree          bool        `json:"thirty_mins_or_free"`
// }

// type Degradation struct {
// 	Mode        int    `json:"mode"`
// 	SupplyState int    `json:"supply_state"`
// 	Cause       string `json:"cause"`
// }

// type cerebroRequest struct {
// 	CustomerLocation struct {
// 		Lat float64 `json:"lat"`
// 		Lng float64 `json:"lng"`
// 	} `json:"customer_location"`
// 	CityID int `json:"city_id"`
// }

// type CustomerInfo struct {
// 	CustomerZoneID      int  `json:"customer_zone_id"`
// 	CustomerZoneRaining bool `json:"customer_zone_raining"`
// }

type cerebroResponse struct {
	ServiceableRestaurants []struct {
		RestaurantID                  string `json:"restaurantId"`
		AreaID                        int    `json:"areaId"`
		CityID                        int    `json:"cityId"`
		ListingServiceabilityResponse struct {
			RestaurantID              int         `json:"restaurant_id"`
			DeliveryTime              int         `json:"delivery_time"`
			MinDeliveryTime           int         `json:"min_delivery_time"`
			MaxDeliveryTime           int         `json:"max_delivery_time"`
			LastMileTravel            float64     `json:"last_mile_travel"`
			DistanceCalculationMethod string      `json:"distance_calculation_method"`
			Serviceability            int         `json:"serviceability"`
			RainMode                  string      `json:"rain_mode"`
			Non_serviceable_reason    interface{} `json:"non_serviceable_reason,omitempty"`
			Degradation               struct {
				Mode        int    `json:"mode"`
				SupplyState int    `json:"supply_state"`
				Cause       string `json:"cause"`
			} `json:"degradation"`
			LongDistance        int     `json:"long_distance"`
			Batchable           bool    `json:"batchable"`
			StartBannerFactor   float64 `json:"start_banner_factor"`
			StopBannerFactor    float64 `json:"stop_banner_factor"`
			DemandShapingFactor float64 `json:"demand_shaping_factor"`
			BannerFactor        float64 `json:"banner_factor"`
			BannerFactorDiff    int     `json:"banner_factor_diff"`
			ZoneBeefup          int     `json:"zone_beefup"`
			RainBeefup          int     `json:"rain_beefup"`
			ThirtyMinsOrFree    bool    `json:"thirty_mins_or_free"`
		} `json:"listingServiceabilityResponse"`
	} `json:"serviceableRestaurants"`
	CustomerInfo struct {
		CustomerZoneID      int  `json:"customer_zone_id"`
		CustomerZoneRaining bool `json:"customer_zone_raining"`
	} `json:"customerInfo"`
}
type Allrestdata struct {
	NonRest     []NonRest `json:"allrestaurantsdata"`
	IsBlackZone bool      `json:"IsBlackZoneEnabled"`
}
type NonRest struct {
	Restid string `json:"restaurant_id"`
	Nsr    string `json:"nonserviceableReason"`
}

type Customer_location struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lng"`
}
type Listing_req struct {
	City_id int64             `json:"city_id"`
	CustLoc Customer_location `json:"customer_location"`
}

// func main() {
// 	router := mux.NewRouter()
// 	router.HandleFunc("/showreststatus", GetRestStatus).Methods("GET")
// 	log.Fatal(http.ListenAndServe(":8001", router))
// 	fmt.Println("doing cerebro now")
// }

func GetRestStatus(w http.ResponseWriter, r *http.Request) {
	endp := r.URL.Query()["url"][0]
	endp = "http://" + endp
	fmt.Println("got url as" + endp)
	latlon := r.URL.Query()["latlon"][0]
	fmt.Println("got latlong as" + latlon)
	latlon = strings.Replace((strings.Trim(latlon, " ")), " ", "", -1)
	city_id := r.URL.Query()["city_id"][0]
	var cust Customer_location
	var listing Listing_req
	lat := strings.Split(latlon, ",")[0]
	lng := strings.Split(latlon, ",")[1]
	latf, _ := strconv.ParseFloat(lat, 7)
	cust.Lat = latf
	lngf, _ := strconv.ParseFloat(lng, 7)
	cust.Lon = lngf
	cityint, _ := strconv.ParseInt(city_id, 10, 64)
	listing.City_id = cityint
	listing.CustLoc = cust
	b, errlist := json.Marshal(listing)
	if errlist != nil {
		fmt.Println(errlist)
	}
	fmt.Println("Printing final json")
	fmt.Print(string(b))
	req, err := http.NewRequest("POST", "http://de-api.swiggy.com/api/delivery/listings", bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		fmt.Println(err)
	}
	resp, err1 := http.DefaultClient.Do(req)
	if err1 != nil {
		fmt.Println(err1)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	fmt.Println("tester")
	fmt.Println(bodyString)
	//fmt.Println(bodyString)
	var finalres cerebroResponse
	json.Unmarshal([]byte(bodyString), &finalres)
	fmt.Println(len(finalres.ServiceableRestaurants))

	var x Allrestdata
	if strings.Contains(strings.ToLower(bodyString), "blackzoneresponse") {
		fmt.Println("PREETESH Checking blackZone")
		x.IsBlackZone = true
	}
	allslice := make([]NonRest, len(finalres.ServiceableRestaurants))
	for i := 0; i < len(finalres.ServiceableRestaurants); i++ {
		restid := finalres.ServiceableRestaurants[i].ListingServiceabilityResponse.RestaurantID
		b := finalres.ServiceableRestaurants[i].ListingServiceabilityResponse.Non_serviceable_reason
		fmt.Println(restid)
		var y NonRest
		if b == nil {
			y.Nsr = "nil"
		} else {
			y.Nsr = fmt.Sprintf("%f", b.(float64))
		}
		y.Restid = strconv.Itoa(restid)
		allslice[i] = y
		fmt.Println(y)
	}
	fmt.Println("final print")
	fmt.Println(len(allslice))
	x.NonRest = allslice
	by, _ := json.Marshal(x)
	fmt.Println(string(by))
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(by))
}
